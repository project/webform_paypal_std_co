/**
 * @file
 * Provides paypal attachment logic.
 */

 (function ($, window, Drupal, drupalSettings, once) {

  'use strict';

  /**
   * Attaches the paypal behavior
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   */
  Drupal.behaviors.webform_paypal_std_co = {
    attach: function (context, settings) {

      for (const base in settings.webform_paypal_std_co.elements) {

        let $element = $('#' + base, context);

        if (!$element.length) {
          continue;
        }
        let $form = $(once('webform_paypal_co_std', $element.closest('form')));

        if (!$form.data('paypal-std-co-element')) {
          $form.data('paypal-std-co-element', base);
        }

        // Make sure we only attach the paypal ckeckout a single
        // element per form.
        if ($form.data('paypal-std-co-element') == base) {
          let paypalCoOptions = {};
          let elementId = $element.attr('id');
          let paypalCoStandardSelectors = JSON.parse($element.attr('data-drupal-paypal-std-co-selectors'));

          const fundingSources = [
            paypal.FUNDING.PAYPAL
          ];

          for (const fundingSource of fundingSources) {
            const paypalButtonsComponent = paypal.Buttons({
              fundingSource: fundingSource,

              // optional styling for buttons
              // https://developer.paypal.com/docs/checkout/standard/customize/buttons-style-guide/
              style: {
                shape: 'rect',
                height: 40,
              },

              // set up the transaction
              createOrder: (data, actions) => {
                // pass in any options from the v2 orders create call:
                // https://developer.paypal.com/api/orders/v2/#orders-create-request-body
                const createOrderPayload = {
                  purchase_units: [
                    {
                      amount: {
                        value: $(paypalCoStandardSelectors['amount'], $form).val(),
                      },
                    },
                  ],
                };

                return actions.order.create(createOrderPayload)
              },

              // Finalize the transaction
              onApprove: (data, actions) => {
                const captureOrderHandler = (details) => {
                  $.ajax({
                    url:  Drupal.url('ajax/webform-paypal-payment/log'),
                    type: "POST",
                    data: {
                      order_id: details.id,
                      intent: details.intent,
                      status: details.status
                    },
                    dataType: "json",
                    success: function (data) {
                      $form.submit();
                    },
                    error: function (error) {
                      $form.submit();
                    }
                  });

                };

                return actions.order.capture().then(captureOrderHandler)
              },

              // handle unrecoverable errors
              // err.order_id needs to be tested.
              onError: (err) => {
                $.ajax({
                  url:  Drupal.url('ajax/webform-paypal-payment/log'),
                  type: "POST",
                  data: {
                    id: err.order_id,
                    status: "FAILED"
                  },
                  dataType: "json",
                  success: function (err) {
                    window.location.reload();
                  },
                  error: function (err) {
                    window.location.reload();
                  }
                });

              },
            });

            if (paypalButtonsComponent.isEligible()) {
              paypalButtonsComponent
                .render('#paypal-button-container')
                .catch((err) => {
                  console.error('PayPal Buttons failed to render');
                });
            } else {
              console.log('The funding source is ineligible');
            }
          }



        }

      }

    }
  };

})(jQuery, window, Drupal, drupalSettings, once);
