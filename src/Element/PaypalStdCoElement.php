<?php

namespace Drupal\webform_paypal_std_co\Element;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Hidden;
use Drupal\Component\Serialization\Json;

/**
 * Provides a 'Paypal Standard Checkout' element.
 *
 * @FormElement("paypal_standard_checkout")
 */
class PaypalStdCoElement extends Hidden {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $class = get_class($this);

    $info['#process'][] = [$class, 'processPaypalStcCoElement'];
    $info['#pre_render'][] = [$class, 'preRenderPaypalStdCoElement'];
    $info['#theme_wrappers'] = [
      'paypal_std_co_element' => [],
      'form_element' => [],
    ];

    // The selectors are gonna be looked within the enclosing form only.
    $info['#paypal_std_co_selectors'] = [];
    return $info;

  }

  /**
   * Prepares a 'paypal_standard_checkout' #type element for rendering.
   */
  public static function preRenderPaypalStdCoElement(array $element) {

    Element::setAttributes($element, ['id']);

    $element['#attributes']['data-drupal-paypal-std-co-selectors'] = Json::encode($element['#paypal_std_co_selectors']);

    return $element;
  }


  /**
   * Processes a 'webform_paypal_std_co_element' element.
   */
  public static function processPaypalStcCoElement(&$element, FormStateInterface $form_state, &$complete_form) {
    $element['#attached']['library'][] = 'webform_paypal_std_co/webform_paypal_std_co.js';

    $settings = [];
    $element['#attached']['drupalSettings']['webform_paypal_std_co']['elements'][$element['#id']] = $settings;

    return $element;
  }

}
