<?php

namespace Drupal\webform_paypal_std_co\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'paypal_standard_checkout' element.
 *
 * @WebformElement(
 *   id = "paypal_standard_checkout",
 *   label = @Translation("Paypal Standard Checkout"),
 *   category = @Translation("Paypal"),
 *   description = @Translation("Provides a placeholder for a Paypal standard checkout integration."),
 * )
 */
class WebformPaypalStdCoElement extends WebformElementBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();

    $extra = [
      'amount' => '',
      'currency' => '',
      'webform_submit_button' => '',
    ];
    $info['#paypal_std_co_selectors'] = $extra + $info['#paypal_std_co_selectors'];

    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    $info = $this->getInfo();
    $properties = [];
    foreach ($info['#paypal_std_co_selectors'] as $key => $value) {
      $properties['paypal_std_co_selectors_' . $key] = $value;
    }
    return $properties + parent::getDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['paypal'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Paypal javascript selectors'),
      '#description' => $this->t('jQuery selectors so that the value of the fields can be used on the paypal element. i.e. %ie. The selectors are gonna be looked within the enclosing form only.', ['%ie' => ':input[name="name[first]"]']),
    ];

    $info = $this->getInfo();
    foreach ($info['#paypal_std_co_selectors'] as $key => $value) {
      $form['paypal']['paypal_std_co_selectors_' . $key] = [
        '#type' => 'textfield',
        '#title' => $this->t(ucfirst(str_replace('_', ' ', $key))),
      ];
      if (in_array($key, ['name', 'first_name', 'last_name'])) {
        $form['paypal']['paypal_std_co_selectors_' . $key]['#description'] = $this->t('You can use either a first and last name, or a full name. Stripe expects a full name so it will be combined on JS.');
      }
    }
    $form['paypal']['paypal_std_co_selectors_amount']['#required'] = TRUE;
    $form['paypal']['paypal_std_co_selectors_currency']['#required'] = TRUE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepare($element, $webform_submission);

    $info = $this->getInfo();
    foreach ($info['#paypal_std_co_selectors'] as $key => $value) {
      if (!empty($element['#paypal_std_co_selectors_' . $key])) {
        $element['#paypal_std_co_selectors'][$key] = $element['#paypal_std_co_selectors_' . $key];
      }
    }
  }

}
