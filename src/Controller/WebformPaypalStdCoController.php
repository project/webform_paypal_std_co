<?php

namespace Drupal\webform_paypal_std_co\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Connection;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handle payment result coming back from Paypal.
 */
class WebformPaypalStdCoController extends ControllerBase {

  /**
   * Database Connection.
   */
  private $connection;

  /**
   * {@inheritdoc}
   */
  public function __construct($connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }


  /**
   * Log payment result.
   */
  public function log(Request $request) {

    try {
      $transaction = $request->request->all();

      if ($transaction['status'] !== 'COMPLETED') {
        \Drupal::logger('webform_paypal_std_co')->warning('An error prevented the buyer from checking out with PayPal. Order id: ' . $request->request->get('order_id'));
        \Drupal::messenger()->addMessage($this->t('An error prevented the buyer from checking out with PayPal.'), 'error');
      } else {
        \Drupal::logger('webform_paypal_std_co')->notice('Paypal payment received. Order id: ' . $request->request->get('order_id'));
        \Drupal::messenger()->addMessage($this->t('Buyer paid with PayPal.'), 'status');
      }

      $this->connection->insert('webform_paypal_std_co')
        ->fields([
          'order_id' => $transaction['order_id'],
          'intent' => $transaction['intent'],
          'status' => $transaction['status'],
        ])
        ->execute();

      return new JsonResponse(['result' => true]);
    }
    catch (\Exception $e) {
      \Drupal::logger('webform_paypal_std_co')->warning('Could not save transation id:' . $request->request->get('id') . ' - Error: ' . $e->getMessage());
      return new JsonResponse(['result' => false]);
    }
  }

}
