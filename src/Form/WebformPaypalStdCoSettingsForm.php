<?php

namespace Drupal\webform_paypal_std_co\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class webform_paypal_std_co Config.
 *
 * @package Drupal\webform_paypal_std_co\Form
 */
class WebformPaypalStdCoSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'webform_paypal_std_co.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_paypal_std_co_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('webform_paypal_std_co.settings');

    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('client_id'),
      '#required' => TRUE,
    ];
    // @todo - Get list of currencies.
    $form['currency'] = [
      '#type' => 'select',
      '#title' => $this->t('Currency'),
      '#options' => ['EUR' => $this->t('EUR'), 'USD' => $this->t('USD')],
      '#default_value' => $config->get('currency'),
      '#required' => TRUE,
    ];
    $form['intent'] = [
      '#type' => 'select',
      '#title' => $this->t('Intent'),
      '#options' => ['capture' => $this->t('Capture'), 'authorize' => $this->t('Authorize')],
      '#default_value' => $config->get('intent'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('webform_paypal_std_co.settings')
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('currency', $form_state->getValue('currency'))
      ->set('intent', $form_state->getValue('intent'))
      ->save();
  }

}
